package com.digitalonus.kafka.springbootkafkaconsumer.model;

import java.io.Serializable;

public class Items implements Serializable {

    private String itemIdentifier;
    private String baseUpc;
    private String description;

    public String getItemIdentifier() {
        return itemIdentifier;
    }

    public void setItemIdentifier(String itemIdentifier) {
        this.itemIdentifier = itemIdentifier;
    }

    public String getBaseUpc() {
        return baseUpc;
    }

    public void setBaseUpc(String baseUpc) {
        this.baseUpc = baseUpc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
