package com.digitalonus.kafka.springbootkafkaconsumer.model;

import java.io.Serializable;

public class Order implements Serializable {

    private String testName;
    private String orderType;
    private Integer expectedTotalRequestsProcessed;
    private Integer expectedReqsSendToAttributions;
    private Integer expectedOnlineOrders;
    private Integer expectedOfflineOrders;
    private Integer expectedTransactionsWithNoGuidKey;
    private Items items;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getExpectedTotalRequestsProcessed() {
        return expectedTotalRequestsProcessed;
    }

    public void setExpectedTotalRequestsProcessed(Integer expectedTotalRequestsProcessed) {
        this.expectedTotalRequestsProcessed = expectedTotalRequestsProcessed;
    }

    public Integer getExpectedReqsSendToAttributions() {
        return expectedReqsSendToAttributions;
    }

    public void setExpectedReqsSendToAttributions(Integer expectedReqsSendToAttributions) {
        this.expectedReqsSendToAttributions = expectedReqsSendToAttributions;
    }

    public Integer getExpectedOnlineOrders() {
        return expectedOnlineOrders;
    }

    public void setExpectedOnlineOrders(Integer expectedOnlineOrders) {
        this.expectedOnlineOrders = expectedOnlineOrders;
    }

    public Integer getExpectedOfflineOrders() {
        return expectedOfflineOrders;
    }

    public void setExpectedOfflineOrders(Integer expectedOfflineOrders) {
        this.expectedOfflineOrders = expectedOfflineOrders;
    }

    public Integer getExpectedTransactionsWithNoGuidKey() {
        return expectedTransactionsWithNoGuidKey;
    }

    public void setExpectedTransactionsWithNoGuidKey(Integer expectedTransactionsWithNoGuidKey) {
        this.expectedTransactionsWithNoGuidKey = expectedTransactionsWithNoGuidKey;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }
}
