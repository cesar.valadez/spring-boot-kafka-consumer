package com.digitalonus.kafka.springbootkafkaconsumer.listener;

import com.digitalonus.kafka.springbootkafkaconsumer.model.Order;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    @KafkaListener(topics = "Kafka_Order_DOU2", groupId = "group_json",
            containerFactory = "orderKafkaListenerFactory")
    public void consumeJson(Order order) throws JsonProcessingException {
        ObjectMapper Obj = new ObjectMapper();
        System.out.println("Consumed JSON Message: ");
        System.out.println(Obj.writeValueAsString(order));
    }
}
